# HW4

## id221012

## Чекпоинты по уровню сложности

- [x] Создать виртуальную машину и запустить на ней "Registry:2"
- [x] Настроить basic-auth для реджистри, используя образ "httpd:2", успешно закачать в реджистри какой-нибудь образ из интернета, например alpine:latest
- [x] Создать docker-compose.yml, который будет запускать этот контейнер с пробросом портов и хранилищем данных - данные реджистри и секреты для basic-auth
- [x] Настроить фронтенд для реджистри
- [x] Автоматизировать все действия через vagrant provisioning
- [x] Научить gitlab-ci складывать образы в наш новый реджистри

[Сдавать сюда](https://docs.google.com/forms/d/e/1FAIpQLSeecvZ20HMEFZ1MjoSNdWdLocvziSmaHD_V7ltQmYnNPItVQw/viewform)

## Список имаджей в реджистри
![plot](./screenshots/Screenshot_from_2022-10-20_12-53-56.png)

## Сертификат
![plot](./screenshots/Screenshot_from_2022-10-20_12-54-11.png)

## Пайплайны
![plot](./screenshots/Screenshot_from_2022-10-20_12-54-44.png)