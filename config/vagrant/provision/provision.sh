#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

echo "+----------------------------------------------------------------+"
export STRING="Initial setup script is running..."
echo $STRING
echo -e "\e[34m Применяем общую конфигурацию для всех виртуальных машин \e[0m"

timedatectl set-timezone Europe/Moscow
timedatectl set-ntp true
apt-get update -y > /dev/null && apt-get upgrade -y > /dev/null
ufw allow OpenSSH
apt -y install wget curl > /dev/null
echo "y" | ufw enable