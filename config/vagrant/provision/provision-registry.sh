#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

# echo -e "$MASTER_IP_ADDR master\n$REPLICA_IP_ADDR replica\n$WP_IP_ADDR wordpress" >> /etc/hosts
echo -e "\e[34m Конфигурация для registry \e[0m"
echo -e "\e[34m Задание 1: \e[0m"
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release > /dev/null

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update > /dev/null
apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin > /dev/null
gpasswd -M root,vagrant docker
# not to waste time
# docker run -d -p 5000:5000 --restart=always --name registry registry:2
echo -e "\e[34m Задание 1 выполнено \e[0m"

systemctl enable docker > /dev/null && systemctl start docker > /dev/null
echo -e "\e[34m Задание 2, 3: \e[0m"
curl -L "https://github.com/docker/compose/releases/download/v2.11.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

mkdir /data
mkdir /auth && cd $_
docker run -d --name httpd2 -p 8080:80 -v "$PWD":/usr/local/apache2/auth httpd:2.4
# > /dev/null

docker exec httpd2 bash -c "echo $DOCKER_PWD | htpasswd -iBc auth/registry.password $DOCKER_USER"


cd /vagrant/registry
docker-compose pull
docker-compose up -d
# docker run -d -p 5000:5000 -e "REGISTRY_AUTH=htpasswd" -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/registry.password -v "$(pwd)":/auth --restart=always --name registry registry:2

echo $DOCKER_PWD | docker login localhost:5000 --username $DOCKER_USER --password-stdin
docker tag registry:2 localhost:5000/privateregistry
docker push localhost:5000/privateregistry
echo -e "\e[34m Задание 2, 3 выполнено. \e[0m"

echo -e "\e[34m Задание 4: \e[0m"
mkdir /auth/certs && cd $_
openssl req -x509 -newkey rsa:4096 -nodes -keyout domain.key -out domain.crt -sha256 -days 365 -subj "/C=RU/ST=Saint Petersburg/L=Saint Petersburg/O=DeusOps Lite/CN=$REGISTRY_DOMAIN" -addext "subjectAltName = DNS:$REGISTRY_DOMAIN, DNS:www.$REGISTRY_DOMAIN"
# add to /etc/default/docker
# DOCKER_OPTS="--insecure-registry https://myregistrydomain.example"
# sudo mkdir -p /etc/docker/certs.d/myregistrydomain.example
# sudo cp /auth/certs/domain.crt /etc/docker/certs.d/myregistrydomain.example/domain.crt
# sudo systemctl restart docker.service
# echo 'registry-password' | docker login myregistrydomain.example --username registry-user --password-stdin
# docker pull ubuntu:latest
# docker tag ubuntu:latest myregistrydomain.example/ubuntu
# docker images -a
# docker push myregistrydomain.example/ubuntu
sh -c 'tee -a /etc/default/docker > /dev/null <<EOF
DOCKER_OPTS="--insecure-registry https://$REGISTRY_DOMAIN"
'EOF
mkdir -p /etc/docker/certs.d/$REGISTRY_DOMAIN
cp /auth/certs/domain.crt /etc/docker/certs.d/$REGISTRY_DOMAIN/ca.crt

sh -c 'tee -a /etc/docker/daemon.json > /dev/null <<EOF
{
    "insecure-registries" : ["https://$REGISTRY_DOMAIN"]
}
'EOF
systemctl restart docker.service

docker pull ubuntu:latest
docker tag ubuntu:latest $REGISTRY_DOMAIN/ubuntu
docker images -a

apt install nginx -y > /dev/null
ufw allow 'Nginx HTTPS'
cp /vagrant/nginx/nginx.conf /etc/nginx/sites-available/$REGISTRY_DOMAIN
cp /auth/certs/* /etc/nginx/conf.d/

ln -s /etc/nginx/sites-available/$REGISTRY_DOMAIN /etc/nginx/sites-enabled/$REGISTRY_DOMAIN
unlink /etc/nginx/sites-enabled/default
sudo nginx -t >/dev/null 2>&1
if [ $? -eq 0 ]; then
    sudo systemctl reload nginx
else
    sudo nginx -t
fi
systemctl restart nginx

echo $DOCKER_PWD | docker login $REGISTRY_DOMAIN --username $DOCKER_USER --password-stdin
docker push $REGISTRY_DOMAIN/ubuntu

# How to test:
# docker pull ubuntu:latest
# docker tag ubuntu:latest localhost:5000/ubuntu:latest
# echo 'registry-password' | docker login localhost:5000 --username registry-user --password-stdin
# echo 'registry-password' | docker login myregistrydomain.example --username registry-user --password-stdin
# docker push localhost:5000/ubuntu:latest
# docker rmi -f 216c552ea5ba
# docker pull localhost:5000/ubuntu:latest
# docker images -a
# curl -X GET http://localhost:5000/v2/_catalog
# https://myregistrydomain.example/v2/_catalog
# curl --user registry-user:registry-password --insecure -I -X GET https://myregistrydomain.example/v2/_catalog
echo -e "\e[34m Задание 4 выполнено. \e[0m"



echo -e "\e[34m Задание 6: \e[0m"

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
apt-get install -y gitlab-runner > /dev/null
gitlab-runner register \
    --non-interactive \
    --url "$URL" \
    --registration-token "$REGISTRATION_TOKEN" \
    --executor "docker" \
    --docker-volumes "/cache" \
    --docker-volumes "/etc/docker/certs.d:/etc/docker/certs.d" \
    --docker-image "docker:20.10.17" \
    --description "docker-runner" \
    --maintenance-note "docker runner from provisioning script" \
    --tag-list "dev-docker" \
    --run-untagged="false" \
    --locked="false" \
    --access-level="not_protected" \
    --docker-privileged

gitlab-runner register \
    --non-interactive \
    --url "$URL" \
    --registration-token "$REGISTRATION_TOKEN" \
    --executor "shell" \
    --description "shell-runner" \
    --maintenance-note "shell runner from provisioning script" \
    --tag-list "dev-shell" \
    --run-untagged="false" \
    --locked="false" \
    --access-level="not_protected"

usermod -aG docker gitlab-runner
systemctl restart gitlab-runner
